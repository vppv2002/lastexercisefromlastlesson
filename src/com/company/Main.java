package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int j;
        System.out.println("Введите количество строк: ");
        int kolStrok = new Scanner(System.in).nextInt();
        String[] arr = new String[kolStrok];
        for (int i = 0; i < kolStrok; i++) {
            System.out.println("Введите " + (i + 1) + "ю строку");
            arr[i] = new Scanner(System.in).nextLine();
        }
        String maxArr = arr[0];
        int numberOfMax = 0;
        for (j=0; j < kolStrok; j++) {
            if (maxArr.length() < arr[j].length())
                maxArr = arr[j];
                numberOfMax=j;
        }
        System.out.println("Самая длинная строка №" + numberOfMax + ", а именно:" + maxArr);
    }
}

